#ifndef _GREEDY_H
#define _GREEDY_H

#include "data.hh"

solution first_fit_decreasing(const instance& inst);
unsigned int ffd_lower_bound(const solution& sol);

#endif

