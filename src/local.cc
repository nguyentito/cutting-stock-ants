#include <algorithm>
#include <cassert>
#include <iostream>

#include "local.hh"

using namespace std;

// Ce truc n'améliore rien s'il est lancé à partir de la solution gloutonne

solution local_search(const instance& inst, const solution& init_sol)
{
    vector<size_t> decreasing(inst.m, 0);
    for (size_t i = 0; i < inst.m; ++i)
        decreasing[i] = i;
    sort(decreasing.begin(), decreasing.end(),
         [&inst](size_t i, size_t j) { return inst.l[i] > inst.l[j]; });

    // longueur restante sur chaque planche
    vector<unsigned int> remaining(init_sol.value, inst.L);

    for (size_t i = 0; i < inst.m; i++)
        for (size_t p = 0; p < init_sol.value; p++)
            remaining[p] -= init_sol.x[i][p] * inst.l[i];

    unsigned int min_p = inst.L - remaining[init_sol.value - 1];
    cout << "initial lenp = " << min_p << endl;
    size_t argmin_p = init_sol.value - 1;
    
    solution cur_sol = init_sol;

    // nouvelle part de chaque planche qu'on va utiliser
    vector<unsigned int> delta(cur_sol.value, 0);

    for (int aaa = 0; aaa < 1; aaa++)
    {

    for (size_t p = 0; p < cur_sol.value; p++)
    {
        fill(delta.begin(), delta.end(), 0);
        bool nofit = false;
        unsigned int lenp = inst.L - remaining[p];

        for (size_t i : decreasing)
        {
            unsigned int b = cur_sol.x[i][p];
            unsigned int l = inst.l[i];

            for (size_t q = 0; b > 0 && q < cur_sol.value; q++)
            {
                if (q == p) continue;
                auto tocut = min(b, (remaining[q] - delta[q]) / l);
                b -= tocut;
                delta[q] += tocut*l;
                lenp -= tocut*l;
            }
            if (b > 0)
                nofit = true;
        }
        if (!nofit) assert(lenp == 0);
        if (lenp == 0) assert(!nofit);
        if (lenp < min_p)
        {
            cout << "found better lenp" << endl;
            min_p = lenp;
            argmin_p = p;
            if (!nofit) {
                cout << "nonofit" << endl;
                break;
            }
        }
        cout << "p = " << p << ", lenp = " << lenp << ", remaining = " << remaining[p] << endl;
    }
    // on ne met à jour cur_sol que quand on a trouvé le déplacement à faire,
    // ce qui économise d'avoir à le copier ou à annuler des changements,
    // mais entraîne un peu de répétition de code
    for (size_t i : decreasing)
    {
        // /!\ ceci est désormais une *référence* mutable
        unsigned int& b = cur_sol.x[i][argmin_p];
        unsigned int l = inst.l[i];

        for (size_t q = 0; b > 0 && q < cur_sol.value; q++)
        {
            if (q == argmin_p) continue;
            auto tocut = min(b, remaining[q] / l);
            b -= tocut;
            cur_sol.x[i][q] += tocut;
            remaining[q] -= tocut*l;
        }
    }
    // dans le cas où une planche n'est plus utilisée, on l'échange pour que
    // les planches utilisées forment un préfixe
    // if (min_p == 0)
    // {
        size_t q = cur_sol.value - 1;
        for (size_t i = 0; i < inst.m; i++)
        {
            swap(cur_sol.x[i][argmin_p], cur_sol.x[i][q]);

        }
        swap(remaining[argmin_p], remaining[q]);
//    }
        if (min_p == 0) cur_sol.value--;
        
    }
    
    return cur_sol;
}


