#include "data.hh"

#include <iostream>
#include <numeric>
#include <stdexcept>

using namespace std;

bool is_valid_solution(const instance& inst, const solution& sol)
{
    try
    {
        if (sol.value > inst.P)
        {
            cerr << "WAT" << endl;
            return false;
        }
        for (size_t p = 0; p < sol.value; p++)
        {
            unsigned int len = 0;
            for (size_t i = 0; i < inst.m; i++)
                len += inst.l.at(i) * sol.x.at(i).at(p);
            if (len > inst.L)
            {
                cerr << "Pieces don't fit into their boards." << endl;
                return false;
            }
        }
        for (size_t i = 0; i < inst.m; i++)
        {
            const auto& row = sol.x.at(i);
            if (accumulate(row.begin(), row.end(), (unsigned)0) < inst.b.at(i))
            {
                cerr << "Unsatisfied demand." << endl;
                return false;
            }
        }
    }
    catch (out_of_range& e)
    {
        cerr << "Out of range." << endl;
        return false;
    }
    return true;
}

void print_solution(const instance& inst, const solution& sol)
{
    cout << "Value = " << sol.value << endl;
    for (size_t p = 0; p < sol.value; p++)
    {
        unsigned int total = 0;
        for (size_t i = 0; i < inst.m; i++)
        {
            if (sol.x[i][p] > 0)
            {
                cout << sol.x[i][p] << "*" << inst.l[i] << " + ";
                total += sol.x[i][p] * inst.l[i];
            }
        }
        cout << "0 = " << total << "/" << inst.L << endl;
    }
}


