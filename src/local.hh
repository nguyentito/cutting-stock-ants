#ifndef _LOCAL_H
#define _GREEDY_H

#include "data.hh"

solution local_search(const instance& inst, const solution& init_sol);

#endif

