#include <algorithm>
#include <numeric>
#include <iostream>
#include <vector>
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <ctime>

#include "data.hh"
#include "greedy.hh"
#include "ant.hh"

using namespace std;

double solve_relaxation(const instance& inst)
{
    double total_length = 0;
    for (size_t i = 0; i < inst.m; i++)
        total_length += inst.l[i] * inst.b[i];
    return total_length / inst.L;
}

double lb_big_elements(const instance& inst)
{
    double m = 0;
    for (unsigned int len : inst.l)
    {
        if (2*len <= inst.L)
            continue;
        double c_big = 0;
        double l_medium = 0;
        for (size_t i = 0; i < inst.m; i++)
        {
            if (inst.l[i] >= len)
                c_big += inst.b[i];
            else if (inst.l[i] > inst.L - len)
                l_medium += inst.l[i] * inst.b[i];
                
        }
        // m = max(m, c_big);
        m = max(m, c_big + l_medium / inst.L);
    }
    return m;
}

int main()
{
    instance inst;
    cin >> inst.P >> inst.L >> inst.m;
    inst.l.resize(inst.m);
    inst.b.resize(inst.m);
    
    for (int i = 0; i < inst.m; i++)
    {
        cin >> inst.l[i] >> inst.b[i];
    }
    cout << "P = " << inst.P << ", ";
    cout << "L = " << inst.L << ", ";
    cout << "m = " << inst.m << endl;
    
    auto sumb = accumulate(inst.b.begin(), inst.b.end(), 0);
    cout << "Total number of objects: " << sumb << endl;

    solution greedy = first_fit_decreasing(inst);
    assert(is_valid_solution(inst, greedy));
    cout << "Greedy solution: " << endl;
    print_solution(inst, greedy);
    
    unsigned int lower_bound_greedy = ffd_lower_bound(greedy);
    cout << "Lower bound from approximation ratio: " << lower_bound_greedy << endl;
    unsigned int lower_bound_relax = ceil(solve_relaxation(inst));
    cout << "Lower bound from linear relaxation:   " << lower_bound_relax << endl;
    unsigned int lower_bound_big = ceil(lb_big_elements(inst));
    cout << "Lower bound from big elements:   " << lower_bound_big << endl;
    unsigned int lower_bound = max(lower_bound_greedy,
                                   max(lower_bound_relax, lower_bound_big));
    assert(lower_bound <= greedy.value);

    solution better = ant(inst, greedy);
    assert(is_valid_solution(inst, better));
    cout << "Solution given by metaheuristic: " << endl;
    print_solution(inst, better);

    return 0;
}

