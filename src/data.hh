#ifndef _DATA_H
#define _DATA_H

#include <vector>
#include <set>

// Structure représentant une instance
// convention : utilisation de size_t pour les tailles de tableau
struct instance
{
    std::size_t P; // nombre de planches disponibles
    unsigned int L; // longueur d'une planche
    std::size_t m; // nombre de pièces
    std::vector<unsigned int> l; // longueur de chaque pièce
    std::vector<unsigned int> b; // demande pour chaque pièce
};

// Représentation d'une solution, où l'on considère qu'on a
// * y_p = 1 pour 0 <= p < value
// * y_p = 0 pour value <= p < P
// Ainsi on réduit les symétries possibles de l'espace des solutions
struct solution
{
    std::size_t value; // nombre de planches utilisées
    // x[i][p] : nombre de pièces de type i découpés dans la planche p
    std::vector<std::vector<unsigned int> > x;
};

bool is_valid_solution(const instance& inst, const solution& sol);
void print_solution(const instance& inst, const solution& sol);

#endif

