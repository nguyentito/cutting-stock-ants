#include "greedy.hh"

#include <algorithm>
#include <cassert>
#include <cmath>

using namespace std;

solution first_fit_decreasing(const instance& inst)
{
    // on trie les pièces dans l'ordre décroissant de l_i / b_i
    vector<size_t> decreasing(inst.m, 0);
    for (size_t i = 0; i < inst.m; ++i)
        decreasing[i] = i;
    sort(decreasing.begin(), decreasing.end(),
         [&inst](size_t i, size_t j) { return inst.l[i] > inst.l[j]; });

    solution sol;
    sol.value = 0;
    sol.x.resize(inst.m);
    
    vector<unsigned int> board_length(inst.P, inst.L);
    for (size_t i : decreasing)
    {
        sol.x[i].resize(inst.P);
        auto b = inst.b[i];
        auto l = inst.l[i];
        size_t p = 0;
        while (b > 0)
        {
            // utilisation d'une nouvelle planche si nécessaire
            if (p >= sol.value)
                sol.value++;
            // nombre de pièces i à tailler dans ce qui reste de la planche p
            auto tocut = min(b, board_length[p] / l);
            b -= tocut;
            board_length[p] -= tocut*l;
            sol.x[i][p] = tocut;
            p++;
        }
    }

    return sol;
}

unsigned int ffd_lower_bound(const solution& sol)
{
    return ceil((9*((double)sol.value-1)) / 11);
}

    
