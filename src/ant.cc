// Implémentation de
// Ant Colony Optimisation and Local Search
// for Bin Packing and Cutting Stock Problems
// John Levine & Frederick Ducatelle, JORS, 2003

#include <algorithm>
#include <cassert>
#include <chrono>
#include <cmath>
#include <iostream>
#include <random>

#include "ant.hh"

using namespace std;


// Paramètres
int n_ants = 100;
int fitness_exponent = 2;
int estimate_exponent = 2;
double evaporation = 0.95;
int n_iterations = 1000;
double init_phero = 1 / (1 - evaporation);
int period = 25;


// Valeur sélective d'une solution
// = moyenne quadratique des taux de remplissage (au carré)
double fitness(const instance& inst, const solution& sol)
{
    size_t n = sol.value;
    double f = 0;
    for (size_t p = 0; p < n; p++)
    {
        double len = 0;
        for (size_t i = 0; i < inst.m; i++)
            len += inst.l.at(i) * sol.x.at(i).at(p);
        f += pow(len, fitness_exponent);
    }
    return f / (pow(inst.L, fitness_exponent) * n);
}

// Heuristique simple pour estimer la valeur d'un élément
double estimate(const instance& inst, unsigned int i)
{
    return pow(inst.l.at(i), estimate_exponent); // taille de l'élément
}

// tire un indice d'un tableau à partir d'une loi de probabilité
size_t draw_index(minstd_rand& rng, const vector<double>& distr)
{
    double total = 0;
    for (double x : distr)
        total += x;

    uniform_real_distribution<double> rand_real(0, total);
    double y = rand_real(rng);

    double x = 0;
    for (size_t i = 0; i < distr.size(); i++)
    {
        x += distr[i];
        if (x >= y)
            return i;
    }
}

solution ant(const instance& inst, const solution& init_sol)
{
    // générateur congruentiel linéaire initialisé avec le temps UNIX
    minstd_rand rng(chrono::system_clock::now().time_since_epoch().count());

    // tableau des phéromones m x m (m: nombre de pièces)
    vector<vector<double>> phero(inst.m, vector<double>(inst.m));

    // phéromones initiales
    for (auto& row : phero)
        for (double& ph : row)
            ph = init_phero;

    // meilleures solutions rencontrées
    double   global_max_fit = fitness(inst, init_sol);
    solution global_max_sol = init_sol;
    double   iter_max_fit = -1;
    solution iter_max_sol;

    // servira de loi de proba pour tirer une pièce à découper
    vector<double> distr(inst.m);

    for (int i_iterations = 0; i_iterations < n_iterations; i_iterations++)
    {
        // chaque fourmi construit une solution
        for (int i_ants = 0; i_ants < n_ants; i_ants++)
        {
            solution cur_sol;
            cur_sol.x.resize(inst.m);
            for (auto& row : cur_sol.x)
                row.resize(inst.P);
            
            size_t p = 0;
            vector<unsigned int> rem_items = inst.b;
            while (true)
            {
                unsigned int rem_len = inst.L;
                unsigned int bin_size = 0;

                bool finished_items = true;
                // sélection du premier élément à partir de l'estimation de valeur
                for (size_t i = 0; i < inst.m; i++)
                {
                    if (rem_items[i] > 0)
                    {
                        finished_items = false;
                        distr[i] = estimate(inst, i);
                    }
                    else
                        distr[i] = 0;
                }
                // cas où rem_items = vecteur nul i.e. tout a été découpé
                if (finished_items) break;

                size_t i0 = draw_index(rng, distr);
                cur_sol.x[i0][p]++;
                rem_items[i0]--;
                rem_len -= inst.l[i0];
                bin_size++;

                // sélection des autres éléments en prenant en compte les phéromones
                while (true)
                {
                    bool finished_len = true;
                    for (size_t i = 0; i < inst.m; i++)
                    {
                        if (rem_items[i] > 0 && inst.l[i] <= rem_len)
                        {
                            finished_len = false;
                            distr[i] = 0;
                            for (int j = 0; j < inst.m; j++)
                                // faut-il compter la multiplicité dans la formule de l'article ?
                                // ici non, mais en fait je sais pas
                                if (cur_sol.x[j][p] > 0)
                                    distr[i] += phero[i][j];
                            // distr[i] /= bin_size;
                            distr[i] *= estimate(inst, i);
                        }
                        else
                            distr[i] = 0;
                    }
                    // cas où aucune pièce ne rentre plus dans la planche actuelle
                    if (finished_len) break;

                    size_t i = draw_index(rng, distr);
                    cur_sol.x[i][p]++;
                    rem_items[i]--;
                    rem_len -= inst.l[i];
                    bin_size++;
                }

                // on commence une nouvelle planche
                p++;
            }
            cur_sol.value = p;

            // on garde la meilleure solution parmi celles produites par les fourmis
            double cur_fit = fitness(inst, cur_sol);
            if (cur_fit > iter_max_fit)
            {
                iter_max_fit = cur_fit;
                iter_max_sol = cur_sol;
            }
        }
        if (iter_max_fit > global_max_fit)
        {
            global_max_fit = iter_max_fit;
            global_max_sol = iter_max_sol;
        }

        // mise à jour des phéromones
        double max_fit;
        solution *max_sol;
        if (i_iterations % period == 0)
        {
            max_fit = global_max_fit;
            max_sol = &global_max_sol;
        }
        else
        {
            max_fit = iter_max_fit;
            max_sol = &iter_max_sol;
        }
        for (size_t i = 0; i < inst.m; i++)
        {
            for (size_t j = 0; j < inst.m; j++)
            {
                phero[i][j] *= evaporation;
                for (size_t p = 0; p < max_sol->value; p++)
                {
                    if (max_sol->x[i][p] > 0 && max_sol->x[j][p] > 0)
                        phero[i][j] += max_fit;
                }
            }
        }
        cout << "Maximum fitness at iteration " << i_iterations << ": " << iter_max_fit << endl;
    }
    return global_max_sol;
}


