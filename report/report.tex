\documentclass[a4paper, 11pt]{article}

% Math symbols, notation, etc.
% Apparently, must be loaded earlier than mathspec?
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{stmaryrd}

% Locale/encoding with XeTeX: UTF-8 by default, use fontspec
%\usepackage{unicode-math}
\usepackage{fontspec}
\usepackage{polyglossia} % Modern replacement for Babel
\setmainlanguage{french} % or {english}
\usepackage{csquotes} % guillemets

% TeX Gyre Pagella = URW Palladio (free Palatino) extended
%\setmainfont[Mapping=tex-text]{TeX Gyre Pagella}
%\setmathfont{Asana Math}

% Other
%\usepackage{fullpage}
%\usepackage{enumerate}
%\usepackage{graphicx}

\begin{document}

\title{Projet métaheuristiques MPRO : problème de découpe}
\author{\textsc{Nguyen} Le Thanh Dung}
\maketitle

\section{Algorithme glouton}

Le problème de découpe peut être vu comme un problème de \emph{bin
  packing} avec $\sum_{i=1}^m b_i$ objets. Nous pouvons donc employer
l'approximation gloutonne \enquote{First Fit Decreasing} :
\begin{itemize}
\item on maintient une liste de planches utilisées, initialisée à la
  liste vide ;
\item on énumère les pièces à découper par taille décroissante ; pour chacune :
  \begin{itemize}
  \item s'il est impossible de la découper dans une des planches
    utilisées, on introduit une nouvelle planche (de longueur pleine) ;
  \item sinon, on emploie la première planche dans la liste qui
    convient, à laquelle on retranche donc la longueur de la pièce.
  \end{itemize}
\end{itemize}

Ici, pour obtenir un algorithme fortement polynomial (donc efficace
même lorsque les $b_i$ sont grands), on va gérer, pour chaque
$i = 1,\ldots,m$, la découpe des $b_i$ pièces de longueur $l_i$
simultanément, tout en maintenant une représentation compacte des
planches restantes.

%  Concrètement :

% \begin{algorithm}
% \caption*{Algorithme glouton pour le problème de découpe}
% \begin{algorithmic}
% \Procedure{FirstFitDecreasing}{$N$, $(P_1, \ldots, P_N)$}
% \State $\{(C_i, R_i, (p_1, \ldots, p_{n_i})\} \gets$ \Call{Voronoi}{$N, (P_1, \ldots, P_N)$} \Comment{Algorithme de Fortune en $O(N \log N)$}
% \State \Comment{où pour tout $i$, $(p_1, \ldots, p_{n_i})$ sont les routeurs à distance $R_i$ du sommet de Voronoi $C_i$}
% \State \Call{Trier}{$\{(C_i, R_i, (p_1, \ldots, p_{n_i})\}$} par rayon décroissant
% \For{$C, R, (p_1, \ldots, p_n) \in \{(C_i, R_i, (p_1, \ldots, p_{n_i})\}$}
% 	\State \Call{Trier}{$\{p_1, \ldots, p_n\}$} dans le sens des aiguilles d'une montre
% 	\If{l'angle $\widehat{p_1 C p_n}$ dépasse 180°} \Comment{Les points ne sont pas confinés dans un demi-disque}
% 		\State \textbf{renvoyer} $R$ \Comment{Il y a existence de jaloux jusqu'à ce que la portée de $p_1, \ldots, p_n$ soit $R$}
% 		\State \Comment{Sinon, on ignore ce sommet de Voronoi et on passe au candidat suivant}
% 	\EndIf
% \EndFor
% \State \textbf{renvoyer} 0

% \EndProcedure
% \end{algorithmic}
% \end{algorithm}

\section{Minoration de la valeur optimale}

\subsection{Par ratio d'approximation}

L'algorithme glouton employé offre la garantie relative de performance
suivante (vue en cours de Complexité et Approximation) :
\[ \textrm{FFD} \leq \frac{11}{9} \textrm{OPT} + 1 \]
où OPT est la valeur de la solution optimale et FFD la valeur de celle
trouvée par l'algorithme.

On en déduit
\[ \textrm{OPT} \geq \left\lceil \frac{9}{11}(\textrm{FFD} - 1)\right\rceil \]
puisque OPT est entier.

\subsection{Par relaxation linéaire}

Établissons une autre minoration à partir du modèle linéaire en
nombres entiers proposé dans l'énoncé.

Soient $x_{ip}$, $i = 1,\ldots,m$, $p = 1,\ldots,p$ et $y_{ip}$,
$p = 1,\ldots,p$, les valeurs prises par les variables dans une
solution réalisable (entière ou rationnelle). On a alors
\begin{alignat*}{3}
  \sum_{p=1}^P Ly_p &\geq \sum_{p=1}^P \sum_{i=1}^m l_ix_{ip} &\qquad \text{en sommant (1)}\\
  L \times \sum_{p=1}^P y_p &\geq \sum_{i=1}^m \sum_{p=1}^P l_ix_{ip} \geq \sum_{i=1}^m b_i
                              &\qquad \text{en sommant (2)}  \\
\end{alignat*}
ce qui fournit la minoration suivante pour la fonction objectif :
\[ \sum_{p=1}^P y_p \geq \frac{1}{L} \sum_{i=1}^m l_ib_i \]
Interprétation : il faut au moins autant de planches que la longueur
totale des pièces demandées divisée par la longueur d'un planche.

Remarquons que si le programme linéaire est réalisable, ce minorant
est nécessairement inférieur à $P$ puisque $y_p \leq 1$ pour tout $P$
dans une solution.  De plus, en posant
\[ x_{ip} = \dfrac{b_i}{P} \qquad\text{ et }\qquad
  y_p = \dfrac{1}{LP} \sum_{i=1}^m l_ib_i \]
  les contraintes (1) et (2) sont satisfaites avec égalité, et on a
  $0 \leq y_p \leq 1$ ce qui est la relaxation continue de la
  contrainte (4). Ce choix de valeur permet d'atteindre le minorant,
  qui est donc \emph{la valeur de la relaxation continue} du modèle
  donné.

Pour le programme en nombre entiers on peut faire un peu mieux en
arrondissant à l'entier supérieur :
\[ \sum_{p=1}^P y_p \geq \left\lceil \frac{1}{L} \sum_{i=1}^m l_ib_i \right\rceil \]


\subsection{Par une méthode ad-hoc}

Il s'agit ici de pouvoir traiter les cas où il y a beaucoup de
\enquote{grosses} pièces à découper, et peu de \enquote{petites}
pièces, de sorte qu'il y ait beaucoup de gaspillage et que par
conséquent la borne ci-dessus soit éloignée du vrai optimum.

Si l'on considère comme \enquote{grosses pièces} celle dont la taille
dépasse $L/2$ ($L$ étant la longueur totale d'une planche), il y aura
au plus une seule de ces pièces par planche dans une solution
réalisable. Le nombre de telles pièces est donc un minorant de la
valeur optimale.

On peut faire mieux. Soit $M > L/2$. Considérons :
\begin{itemize}
\item d'une part, le multi-ensemble $G_M$ des pièces de taille $l_i$
  avec $l_i \geq M$, comptées avec multiplicité~;
\item d'autre part, le multi-ensemble $P_M$ des pièces de taille $l_i$
  avec $L - M \leq l_i < M$, comptées avec multiplicité.
\end{itemize}
$G_M$ et $P_M$ sont disjoints et leur réunion est comprise dans le
multi-ensemble total des pièces à découper. De plus, les pièces de $G_M$
et celles de $P_M$ occuperont deux ensembles de planches distincts dans
toute solution réalisable, en effet tout couple de pièces de
$G_M \times P$ a une longueur totale dépassant $L$. On peut minorer
\begin{itemize}
\item le nombre de pièces employées pour découper $G_M$ par le cardinal
  de $G_M$
\item le nombre de pièces employées pour découper $P_M$ par le calcul de
  la relaxation linéaire
\end{itemize}
Soit le minorant suivant :
\[ \sum_{p=1}^P y_p \geq \sup_{M > L/2} \lvert G_M \rvert +
   \left\lceil \frac{1}{L} \sum_{i \in P_M} l_ib_i \right\rceil \]

\section{Métaheuristique : optimisation par colonie de fourmis}

J'ai repris ici l'algorithme décrit dans l'article \enquote{Ant Colony
  Optimisation and Local Search for Bin Packing and Cutting Stock
  Problems} par John Levine et Frederick Ducatelle. Plus précisément,
j'implémente l'approche \enquote{Pure ACO}, c'est-à-dire sans combiner
avec de la recherche locale.

Le principe est le suivant : l'algorithme maintient une matrice $\tau$
où $\tau(i,j)$ représente l'affinité des pièces $i$ et $j$, autrement
dit, cette donné quantifie la tendance qu'on aura à mettre les pièces
$i$ et $j$ ensemble dans une planche. Par analogie avec l'application
des colonies de fourmis au voyageur du commerce, les $\tau(i,j)$ sont
appelés phéromones.

À chaque itération on va construire indépendamment un certain nombre
(constant) de solutions réalisables du problème. La construction
s'effectue planche par planche, en découpant à chaque fois une pièce
tirée au hasard selon une loi où la probabilité d'obtenir la pièce $j$
est proportionnelle au produit :
\begin{itemize}
\item de la somme des affinités $\tau(i,j)$ entre $j$ et les pièces
  $i$ déjà découpées dans cette planche,
\item et d'une estimation simple, ne dépendant pas de l'histoire du
  calcul, de l'utilité d'inclure $j$.
\end{itemize}

Ensuite, parmi les solutions construites, on retient celle qui
présente la meilleure valeur sélective (ou \textit{fitness}) selon un
critère à définir, et on met à jour les phéromones :
\begin{itemize}
\item d'abord par évaporation c'est-à-dire multiplication par un
  facteur $< 1$ ;
\item puis par renforcement (additif) des associations entre pièces
  présentes sur des planches communes dans la solution de
  \textit{fitness} maximale parmi la population générée durant cette
  itération.
\end{itemize}

Les paramètres sont donc :
\begin{itemize}
\item le nombre d'itérations ;
\item le nombre de \enquote{fourmis}, c'est-à-dire de solutions
  construites à chaque itération ;
\item la fonction de \textit{fitness} d'une solution ;
\item l'estimation de l'utilité d'une pièce ;
\item le facteur d'évaporation.
\end{itemize}

Ici, la \textit{fitness} est la moyenne quadratique de la proportion
de chaque planche qui est remplie. L'emploi d'une moyenne quadratique,
plutôt qu'une simple moyenne arithmétique, permet de pondérer plus
fortement les planches bien remplies : on espère ainsi en vider
progressivement d'autres et parvenir à des solutions avec moins de
planches. En effet, à fonction objectif (nombre de planches) égale,
les solutions de meilleure \textit{fitness} sont celles qui
contiennent le plus de variation entre les planches en proportion de
remplissage, et donc celles qui sont \enquote{les plus proches} de
solutions avec moins de planches.

Quant à l'estimation des pièces, c'est simplement le carré de leur
longueur. En fait, dans la construction de la population de solutions,
si le choix est déterministe et porte toujours sur le plus grand
élément, en négligeant donc les phéromones, on retombe sur la solution
construire par First Fit Decreasing lorsque l'estimation est une
fonction croissante de la longueur.

Au cours de ses itérations, l'algorithme maintient la solution de
meilleure \textit{fitness} rencontrée pour la renvoyer à la
fin. Disposant d'une solution gloutonne au problème calculée
précédemment, j'ai choisi d'initialiser la recherche en fournissant
comme maximum global de \textit{fitness} cette solution au départ.

\section{Résultats expérimentaux}

\subsection{Instances fournies}

\begin{tabular}{|c|c|c|c|c|c|}
  \hline
  n\textsuperscript{o} & Nom du fichier & $P$ & $L$ & $m$ & Nombre total de pièces \\
  \hline
  1 & \texttt{cut10250.dat} & 1000 & 250 & 10 & 1542 \\
  2 & \texttt{cut10600.dat} & 1000 & 600 & 10 & 1542 \\
  3 & \texttt{cut20250.dat} & 3000 & 250 & 20 & 2977 \\
  4 & \texttt{cut20600.dat} & 1500 & 600 & 20 & 2977 \\
  \hline
\end{tabular}

\subsection{Solutions obtenues}

\begin{tabular}{|l|c|c|c|c|}
  \hline
  n\textsuperscript{o} de l'instance & 1 & 2 & 3 & 4 \\
%  \hline
  Meilleur minorant & 650 & 547 & 1804 & 801 \\
%  \hline
  Valeur de la solution gloutonne & 700 & 481 & 1815 & 841 \\
%  \hline
  Valeur atteinte par l'heuristique & 690 & 474 & 1815 & 838 \\
%  \hline
  Temps de calcul de l'heuristique & 3m12s & 3m35s & 10m54s & 16m7s \\
  \hline
\end{tabular}

Je n'ai pas testé plusieurs valeurs des paramètres (les valeurs qui
ont été employées sont celles écrites dans le code source fourni), ou
relevé plusieurs essais de l'heuristique (celle-ci étant randomisée,
cela aurait été nécessaire pour avoir des résultats significatifs).

On voit cependant qu'avec 1000 itérations (un nombre relativement
petit, choisi pour se limiter à des temps de calcul de l'ordre de
quelques minutes), on n'a que très peu d'amélioration par rapport à
l'algorithme glouton (entre 1~\% et 2~\% sur les deux premières
instances). Dans l'instance 3 les colonies de fourmis ne trouvent même
pas de meilleure solution que le glouton~; cependant, dans ce cas-là,
la proximité au minorant calculé peut mener à conjecturer que 1815 est
la valeur optimale.

Un problème de cette heuristique est que les solutions à chaque
itération sont construites élément par élément. Par conséquent, le
temps de calcul (à nombre d'itérations fixé) est proportionnel au
nombre total de pièces à découper, celui-ci étant de l'ordre de $10^2$
fois plus grand que le nombre de types de pièces distincts sur les
instances fournies. Un algorithme qui exploiterait mieux la tendance
des bonnes solutions à voir apparaître souvent des motifs de découpe
répétés éviterait du travail redondant et pourrait donc être plus
rapide.

\subsection{Comparaison des minorants}

\begin{tabular}{|l|c|c|c|c|}
  \hline
  n\textsuperscript{o} de l'instance & 1 & 2 & 3 & 4 \\
  Minorant par algorithme d'approximation & 572 & 393 & 1485 & 688 \\
  Minorant par relaxation linéaire & 650 & 450 & 1491 & 801 \\
  Minorant par méthode ad-hoc & 547 & N/A & 1804 & N/A \\
  \hline
\end{tabular}

L'indication N/A indique que la méthode ad-hoc ne fournit aucun
minorant. Cela se produit quand aucune des tailles des pièces ne
dépasse la moitié de la longueur d'une planche.

On observe que sur les 4 instances fournies, le minorant par
relaxation linéaire bat systématiquement celui par l'algorithme
d'approximation. La méthode ad-hoc est globalement mauvaise sauf dans
le cas de la 3ème instance où elle bat significativement les autres et
est extrêmement proche de la solution réalisable trouvée par le
glouton.

\section{Description des sources fournies et du programme}

\begin{itemize}
\item \texttt{main.\{cc,hh\}} : point d'entrée du programme
\item \texttt{data.\{cc,hh\}} : structures de données communes au programme (instance et solution)
\item \texttt{greedy.\{cc,hh\}} : algorithme glouton (+ calcul d'un minorant)
\item \texttt{ant.\{cc,hh\}} : implémentation de l'optimisation par colonies de fourmis
\end{itemize}

La lecture des instances se fait sur l'entrée standard et le résultat
est affiché sur la sortie standard. Les mesures de performance ont été
effectuées avec la commande Unix \texttt{time}.

\end{document}
